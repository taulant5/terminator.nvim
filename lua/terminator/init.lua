local api = vim.api
local buf = {}
--maybe make this configurable
local lastBuf = 1
local win


--[[
Returns a buffer instance
Unless deleted, it should always be the same buffer
--]]
local function getBufferInstance(n)
    if (not buf[n] or not api.nvim_buf_is_valid(buf[n])) then
        buf[n] = api.nvim_create_buf(false, true)
        api.nvim_buf_set_option(buf[n], 'bufhidden', 'hide')
        api.nvim_buf_call(buf[n], function() api.nvim_command("call termopen($SHELL)") end)
        end
    return buf[n]
end


--[[
Helper function to create windows
--]]
local function createWindow(bufferInstance)
  -- get dimensions
  local width = api.nvim_get_option("columns")
  local height = api.nvim_get_option("lines")
  -- calculate our floating window size
  local win_height = math.ceil(height * 0.8 - 4)
  local win_width = math.ceil(width * 0.8)

  -- and its starting position
  local row = math.ceil((height - win_height) / 2 - 1)
  local col = math.ceil((width - win_width) / 2)

  local opts = {
    style = "minimal",
    relative = "editor",
    width = win_width,
    height = win_height,
    row = row,
    col = col
  }

  win = api.nvim_open_win(bufferInstance, true, opts)
  return win
end

local function isOpen() 
    return win and api.nvim_win_is_valid(win)
end

local function openWindow(n)
    if (not isOpen()) then
        win = createWindow(getBufferInstance(n))
        lastBuf = n
    end
    return win
end

local function closeWindow()
    if (isOpen()) then
        api.nvim_win_close(win, true)
    end
end


local function toggleWindow()
    if isOpen() then
        closeWindow()
        return
    end
    openWindow(lastBuf)
end


return
{
    openWin = openWindow,
    closeWin = closeWindow,
    isOpen = isOpen,
    toggleWin = toggleWindow
}
