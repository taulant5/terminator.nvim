# [WIP]terminator.nvim
Launch multiple terminalbuffers in floating windows and move them into background.

Written in lua, no config. !WIP!

## mappings
example mappings:
 ```
 api.nvim_set_keymap('t', '<Esc>', '<C-\\><C-n>', { noremap = true, silent = true})
 api.nvim_set_keymap('n', '<C-t>1', '<cmd>lua require("terminator").openWin(1)<CR>',{ noremap = true, silent = true})
 api.nvim_set_keymap('n', '<C-t>2', '<cmd>lua require("terminator").openWin(2)<CR>',{ noremap = true, silent = true})
 api.nvim_set_keymap('n', '<C-t>3', '<cmd>lua require("terminator").openWin(3)<CR>',{ noremap = true, silent = true})
 api.nvim_set_keymap('n', '<C-t>4', '<cmd>lua require("terminator").openWin(4)<CR>',{ noremap = true, silent = true})
 api.nvim_set_keymap('n', '<C-t>t', '<cmd>lua require("terminator").toggleWin()<CR>',{ noremap = true, silent = true})
 api.nvim_set_keymap('t', '<C-t>t', '<cmd>lua require("terminator").toggleWin()<CR>',{ noremap = true, silent = true})
 ```

